import Data.List
import Data.Function ((&))
finalRow xs
  | ((filter (== Nothing) xs & length) == 1) &&
    ((filter (/= Nothing) xs & sort & group & length) == 1)
  = sort xs & drop 1 & ((sort xs !! 1) :)
  | otherwise = xs
