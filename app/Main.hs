module Main where
import Data.List (group,sort,transpose,intercalate)
import Data.Function ((&))

isGameOver :: [[Maybe Char]] -> (Bool,Maybe Char)
isGameOver xs
  | rowWin xs /= Nothing  = (True,rowWin xs)
  | columnWin xs /= Nothing = (True,columnWin xs)
  | lDiagonalWin xs /= Nothing = (True,lDiagonalWin xs)
  | rDiagonalWin xs /= Nothing = (True,rDiagonalWin xs)
  | otherwise = (False,Nothing)

rowWin :: [[Maybe Char]] -> Maybe Char
rowWin xs
  | null $ test xs = Nothing
  | length(test xs) == 1 =(head(head(test xs)))
  | otherwise = error
  "Unreachable state (more than one player has \"won\" the game)"
  where
    test ys = map (map head . group . sort) ys & filter(\x -> length x == 1)

columnWin :: [[Maybe Char]] -> Maybe Char
columnWin xs = rowWin $ transpose xs

getDiagonal :: [[a]] -> [a]
getDiagonal [[]] = []
getDiagonal [xs] = [head xs]
getDiagonal xs = head(head xs) : getDiagonal (map tail (tail xs))

lDiagonalWin :: [[Maybe Char]] -> Maybe Char
lDiagonalWin xs
  | length(test xs) == 1 = head(test xs)
  | otherwise = Nothing
  where
    test xs = getDiagonal xs & sort & group & map head

rDiagonalWin :: [[Maybe Char]] -> Maybe Char
rDiagonalWin xs = lDiagonalWin $ map reverse xs

addVerticalBar :: Char -> String
addVerticalBar x = x : " | "

convert :: Maybe Char -> Char
convert (Just a) = a
convert Nothing = ' '

prettify :: [[Maybe Char]] -> String
prettify xs = 
  map((++"\n") . reverse . drop 3 . reverse
      . concatMap (addVerticalBar . convert)) xs & intercalate "---------\n"

main :: IO ()
main = do
  let a = [[Just 'X',Just 'X',Just 'X'],[Nothing,Just 'X',Just 'O']
          ,[Nothing,Just 'O',Just 'O']]
  putStr "\n"
  putStr (prettify a)
